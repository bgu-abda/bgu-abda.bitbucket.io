### A Pluto.jl notebook ###
# v0.14.7

using Markdown
using InteractiveUtils

# ╔═╡ 945e903e-bef6-11eb-345b-b59e7c7cfeb2
begin
	using CSV, DataFrames, Turing, StatsPlots
	gr()
end;

# ╔═╡ ffc4e0ad-cdb5-4a43-9ed3-423f3331d824
md"# Salaries in the city of Norfolk municipality

_A case study of hierarchical Bayesian modeling._
"

# ╔═╡ 7527db28-3aac-403c-b09e-b05901e0cf5f
md"## Data

The data is a list of employees. We consider here the salary as the random variable of interest and department and employee status as explanatory variables.
"

# ╔═╡ 95326c68-aadd-4c7c-888c-6b3c1f44ec3d
df = CSV.File("02norfolk_employee_data.csv") |> DataFrame

# ╔═╡ f876ff36-2a87-4218-b4f4-5bb8c9226cd0
	md"Some of the salaries are per hour, while others are per year. Fortunately, the highest hourly rate is much lower than the lowest yearly salary, hence we can ‘fix’ the data by scaling up hourly rates by the number of work hours per year (≈ 2000).

We find the threshold separating hourly and yearly salaries as the geometric mean of the two ends of the longest interval between subsequent sorted salaries:
"

# ╔═╡ 9354f06d-a6ef-4116-9a17-3e6ef7cdddd8
begin
	salary = df[!, Symbol("Base Salary")]
	department = df[!, Symbol("Department  ")]
	status = df[!, Symbol("Employee Status")]
	threshold = let salary = copy(salary)
		sort!(salary)
		d = salary[2:end] ./ salary[begin:end-1]
		imax = findmax(d)[2]
		threshold = sqrt(salary[imax]*salary[imax+1])
	end	
end

# ╔═╡ 62270bf9-a140-4c83-84ea-c9f18b8b20a2
let
	histogram(log.(salary[salary .< threshold]), type=:density, label="hourly", color="lightblue", xlabel="log salary")
	histogram!(log.(salary[salary .>= threshold]), type=:density, label="yearly", color="lightgreen")
	vline!(log.([threshold]), label="threshold", lw=2, color="red")
end

# ╔═╡ b21f516d-6d24-4b56-af3c-1a49d038ae04
md"With hourly salaries multiplied by the number of work hours in a year, the distribution of salaries is unimodel."

# ╔═╡ 1949496e-6406-45cc-9fff-d6a1b9e452fd
yearly_salary = let 
	hours_per_year = 40*52 # 40 hours per week, 52 weeks
	yearly = copy(salary)
	yearly[yearly .< threshold] .*= hours_per_year
	yearly
end;

# ╔═╡ 42a9a058-b50c-421c-8823-26a7e3427f6b
histogram(log.(yearly_salary), normalize=:pdf, xlabel="log salary", label="yearly salary")

# ╔═╡ def4392f-984e-4089-b4a3-86661347fd61
md"One can see from the plots that the distribution of all salaries looks more like a Gauss' bell on the log scale. Because of that, we log-transform the data. Afterwards, we standardize the data to zero mean and unit variance.
"

# ╔═╡ 0806aabf-15e3-411b-89ad-49f11bf7ec11
begin
	log_yearly_salary = log.(yearly_salary);
	log_yearly_salary_mean = mean(log_yearly_salary)
	log_yearly_salary_std = std(log_yearly_salary)
	log_yearly_salary = (log_yearly_salary .- log_yearly_salary_mean) ./
						log_yearly_salary_std
end;

# ╔═╡ 11ba8d6d-83de-49da-b923-19a95c0844dd
md"For convenience, the data is transformed into three arrays:

* salary --- the target variable,
* department --- an explanatory variable,
* status --- employee status, another explanatory variable.

We collect the data in two forms: with dependence only on the department, and on both the department and the employee status.
"

# ╔═╡ 5f7b6c9a-2759-4be8-b0a6-479bf179fe9a
begin
	function list2map(list)
		map = Dict{String, Int}()
		for (i, n) in enumerate(unique(list))
			map[n] = i
		end
		map
	end
	deptmap = list2map(department)
	statmap = list2map(status)
end;

# ╔═╡ 8cd856ad-e0ea-4be5-a926-0c54bac6ffa2
begin
	deptidx = map((d) -> deptmap[d], department)
	statidx = map((s) -> statmap[s], status)
end;

# ╔═╡ a0c08876-180e-44f3-a0e1-02ba8a73bd4d


# ╔═╡ 959a3a25-e20d-44a0-800a-e8d331685482
md"## Models

We define two models, `model_by_dept` and `model_by_dept_stat`. `model_by_dept` is conditioned only on the department. `model_by_dept_stat` is conditioned on both the department and on employee status.
"

# ╔═╡ 99de2147-cf15-4d1c-b2ae-554c63c4f88d
md"### Model by department only

To speed up the inference, instead of conditioning on each employee individually, we collect mean, variance, and count for each department. For a normal distribution, empirical mean $m$ and empirical variance $s^2$ are distributed as (see [Wikipedia](https://en.wikipedia.org/wiki/Normal_distribution)):

$$m \sim \mathrm{Normal}\left(\mu, \frac \sigma {\sqrt n}\right)$$
$$s^2 \sim \mathrm{Gamma}\left(\frac {n-1} 2, \frac {2\sigma^2} {n-1}\right)$$


Conditioning on individual employees would work too but take significantly more time — we would have $\approx 2000$ observation points instead of just $165$.
"

# ╔═╡ cc95913c-1dee-475c-821b-618547cce8c4
begin
	by_dept_mean = Array{Float64, 1}(undef, length(deptmap))
	by_dept_var = Array{Float64, 1}(undef, length(deptmap))
	by_dept_count = Array{Float64, 1}(undef, length(deptmap))
	for i in 1:length(deptmap)
		by_dept = log_yearly_salary[deptidx .== i]
		by_dept_mean[i] = mean(by_dept)
		by_dept_var[i] = var(by_dept)
		by_dept_count[i] = length(by_dept)
	end
end

# ╔═╡ 5b5a6480-e228-421c-965c-e9623c99a2ac
md"Conditioning on variance can cause problems of two kinds. First, for less than 2 samples, the variance is undefined. Then, if all salaries are the same, the variance is zero, and the density of Gamma distribution on 0 for k > 1 is 0. 


Therefore, we only condition on variance if there are enough samples (more than 3) and the values of the samples are different. This constraint is an indirect consequence of our use of LogNormal approximation of the distribution of salaries.
"

# ╔═╡ cdf389cc-92e5-426d-b944-ad39312fa2ad
@model function model_by_dept(mean, var, count)
	μμ ~ Normal(0, 1)
	σμ ~ Gamma(1, 1)
	μσ ~ Normal(0, 1)
	σσ ~ Gamma(1, 1)
	μ ~ MvNormal(fill(μμ, length(mean)), σμ)
	σ ~ MvLogNormal(fill(μσ, length(mean)), σσ)
	for i in eachindex(mean)
		σ2 = σ[i]^2
		mean[i] ~ Normal(μ[i], sqrt(σ2/count[i]))
		k = (count[i]-1)/2 
		if k > 1 && var[i] > 0 
			var[i] ~ Gamma(k, σ2/k)
		end
	end
end

# ╔═╡ 7efcb89c-4341-405e-9cb4-ed9ff5f3348a
chain_by_dept = let NSAMPLES = 1000 
	NBURN = NSAMPLES
	sample(
		model_by_dept(by_dept_mean, by_dept_var, by_dept_count),
		HMC(0.0005, 5), 
		NBURN + NSAMPLES)[NBURN + 1:end]
end;

# ╔═╡ 67620a9f-bdaa-470b-bd97-d2455c8c8a26
md" Inference on the hierarchical model over department discovers _shrinking_ of the department means toward the population mean. This is an anticipated result. There is obviously less variance between average department salaries than between individual employees."

# ╔═╡ e87a9d59-6c30-4ff0-b2ef-328e51fd39df
begin
	histogram(
		log_yearly_salary .* log_yearly_salary_std .+ log_yearly_salary_mean, 
		label="employee", 
		normalize=:pdf, 
		xlabel="log yearly salary",
		alpha=0.75)
	histogram!(
		mean(Array(group(chain_by_dept, :μ)), dims=1)[1, :] .* log_yearly_salary_std .+ log_yearly_salary_mean, 
		label="department",
		normalize=:pdf,
		alpha=0.75)
end

# ╔═╡ b629bb7d-2069-4084-9756-89b6d8b18f5a
md"### Model by department and status

In this model, just like before, we group observations and compute summaries. Since most groups (by **both** department and status) are small --- , we drop variances, and condition on means only. 
"

# ╔═╡ d066e840-fa74-4dc6-8b28-7bfc56217afb
begin
	by_dept_stat_mean = Array{Float64, 2}(undef, length(deptmap), length(statmap))
	by_dept_stat_count = Array{Float64, 2}(undef, length(deptmap), length(statmap))
	for i in 1:length(deptmap)
		for j in 1:length(statmap)
			by_dept_stat = log_yearly_salary[(deptidx .== i) .& (statidx .== j)]
			by_dept_stat_mean[i, j] = mean(by_dept_stat)
			by_dept_stat_count[i, j] = length(by_dept_stat)
		end
	end
end

# ╔═╡ 33df2f51-b1e0-4dbc-a074-e177fe55bedb
md"To convince ourselves that the counts are small, let's compute the median of non-zero counts:"

# ╔═╡ ece5a171-3a54-4c68-8d1e-21c7bf9a7dc5
median_count = median(by_dept_stat_count[by_dept_stat_count .> 0])

# ╔═╡ e986e4d2-20cf-49de-b487-10e06388dcab
md"With the median value of just $median_count it does not make much sense to waste time on conditioning on the variance."

# ╔═╡ 49a34ec6-79ca-4d7a-b19f-2d2d74be18a4
md"In the model, we must combine the influence of department and status. A simple option is just to sum up the means, but a better approach is to compute an average of the means weighted by reciprocals of variances:

$$\mu = \frac {\frac {\mu_1} {\sigma_1^2} + \frac {\mu_2} {\sigma_2^2}} {\frac 1 {\sigma_1^2} + \frac 1 {\sigma_2^2}}$$

This approach has mathematical justification, which we leave to the reader to discover (by search or derivation)."

# ╔═╡ 7e186065-0b68-4576-b445-186ece44858b
@model function model_by_dept_stat(mean, count)
	# department
	μμd ~ Normal(0, 1)
	σμd ~ Gamma(1, 1)
	μσd ~ Normal(0, 1)
	σσd ~ Gamma(1, 1)
	μd ~ MvNormal(fill(μμd, size(mean)[1]), σμd)
	σd ~ MvLogNormal(fill(μσd, size(mean)[1]), σσd)
	
	μμs ~ Normal(0, 1)
	σμs ~ Gamma(1, 1)
	μσs ~ Normal(0, 1)
	σσs ~ Gamma(1, 1)
	μs ~ MvNormal(fill(μμs, size(mean)[2]), σμs)
	σs ~ MvLogNormal(fill(μσs, size(mean)[2]), σσs)
	
	for i in 1:size(mean)[1]
		for j in 1:size(mean)[2]
			if count[i, j] > 0
				μ = (μd[i]/σd[i]^2 + μs[j]/σs[j]^2)/(1/σd[i]^2 + 1/σs[j]^2)
				σ2 = 1/(1/σd[i]^2 + 1/σs[j]^2)
				mean[i, j] ~ Normal(μ, sqrt(σ2/count[i,j]))
			end
		end
	end
end

# ╔═╡ 31edafc0-7c9a-4aed-8e8f-5bf8a6707c9a
chain_by_dept_stat = let NSAMPLES = 1000
	NBURN = NSAMPLES
	sample(
		model_by_dept_stat(by_dept_stat_mean, by_dept_stat_count),
		HMC(0.0005, 5), 
		NBURN+NSAMPLES)[NBURN + 1:end]
end;

# ╔═╡ 61937be1-2492-4895-953a-e7474669c560
begin
	histogram(
		log_yearly_salary .* log_yearly_salary_std .+ log_yearly_salary_mean, 
		label="employee", 
		normalize=:pdf, 
		xlabel="log yearly salary",
		alpha=0.75)
	histogram!(
		mean(Array(group(chain_by_dept_stat, :μd)), dims=1)[1, :] .* log_yearly_salary_std .+ log_yearly_salary_mean, 
		label="department",
		normalize=:pdf,
		alpha=0.75)
	histogram!(
		mean(Array(group(chain_by_dept_stat, :μs)), dims=1)[1, :] .* log_yearly_salary_std .+ log_yearly_salary_mean, 
		label="status",
		normalize=:pdf,
		alpha=0.75)
end

# ╔═╡ 4d5ae1f6-a71b-4cbd-a461-379b42eb53f7
md"Specifying an 'employee status only' model is left as an exercise to the reader.
"

# ╔═╡ Cell order:
# ╟─ffc4e0ad-cdb5-4a43-9ed3-423f3331d824
# ╠═945e903e-bef6-11eb-345b-b59e7c7cfeb2
# ╟─7527db28-3aac-403c-b09e-b05901e0cf5f
# ╠═95326c68-aadd-4c7c-888c-6b3c1f44ec3d
# ╟─f876ff36-2a87-4218-b4f4-5bb8c9226cd0
# ╠═9354f06d-a6ef-4116-9a17-3e6ef7cdddd8
# ╠═62270bf9-a140-4c83-84ea-c9f18b8b20a2
# ╟─b21f516d-6d24-4b56-af3c-1a49d038ae04
# ╠═1949496e-6406-45cc-9fff-d6a1b9e452fd
# ╠═42a9a058-b50c-421c-8823-26a7e3427f6b
# ╟─def4392f-984e-4089-b4a3-86661347fd61
# ╠═0806aabf-15e3-411b-89ad-49f11bf7ec11
# ╟─11ba8d6d-83de-49da-b923-19a95c0844dd
# ╠═5f7b6c9a-2759-4be8-b0a6-479bf179fe9a
# ╠═8cd856ad-e0ea-4be5-a926-0c54bac6ffa2
# ╠═a0c08876-180e-44f3-a0e1-02ba8a73bd4d
# ╟─959a3a25-e20d-44a0-800a-e8d331685482
# ╟─99de2147-cf15-4d1c-b2ae-554c63c4f88d
# ╠═cc95913c-1dee-475c-821b-618547cce8c4
# ╟─5b5a6480-e228-421c-965c-e9623c99a2ac
# ╠═cdf389cc-92e5-426d-b944-ad39312fa2ad
# ╠═7efcb89c-4341-405e-9cb4-ed9ff5f3348a
# ╟─67620a9f-bdaa-470b-bd97-d2455c8c8a26
# ╠═e87a9d59-6c30-4ff0-b2ef-328e51fd39df
# ╟─b629bb7d-2069-4084-9756-89b6d8b18f5a
# ╠═d066e840-fa74-4dc6-8b28-7bfc56217afb
# ╟─33df2f51-b1e0-4dbc-a074-e177fe55bedb
# ╠═ece5a171-3a54-4c68-8d1e-21c7bf9a7dc5
# ╟─e986e4d2-20cf-49de-b487-10e06388dcab
# ╟─49a34ec6-79ca-4d7a-b19f-2d2d74be18a4
# ╠═7e186065-0b68-4576-b445-186ece44858b
# ╠═31edafc0-7c9a-4aed-8e8f-5bf8a6707c9a
# ╠═61937be1-2492-4895-953a-e7474669c560
# ╟─4d5ae1f6-a71b-4cbd-a461-379b42eb53f7
