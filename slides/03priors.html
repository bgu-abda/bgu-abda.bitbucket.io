<!doctype html>
<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">

		<title>ABDA: Priors</title>

		<link rel="stylesheet" href="css/reveal.css">
		<link rel="stylesheet" href="css/theme/white.css">

		<!-- Theme used for syntax highlighting of code -->
		<link rel="stylesheet" href="lib/css/arduino-light.css">

		<!-- Printing and PDF exports -->
		<script>
			var link = document.createElement( 'link' );
link.rel = 'stylesheet';
link.type = 'text/css';
link.href = window.location.search.match( /print-pdf/gi ) ? 'css/print/pdf.css' : 'css/print/paper.css';
document.getElementsByTagName( 'head' )[0].appendChild( link );
		</script>
		<style type="text/css">
.reveal .slides section .static { visibility: hidden; height: 0; width: 0} /* replace with animated for PDF export */

.reveal .slides section .highlight-current-bg-cyan,
.reveal .slides section .highlight-current-bg-magenta,
.reveal .slides section .fragment.highlight-current-bg-yellow,
.reveal .slides section .highlight-bg-cyan,
.reveal .slides section .highlight-bg-magenta,
.reveal .slides section .fragment.highlight-bg-yellow {
	opacity: 1;
	visibility: inherit; }

		.reveal .slides section .highlight-bg-cyan.visible {
			background-color: cyan;
		}
		.reveal .slides section .highlight-bg-magenta.visible {
			background-color: magenta;
		}
		.reveal .slides section .fragment.highlight-bg-yellow.visible {
			background-color: yellow;
		}
		.reveal .slides section .highlight-current-bg-cyan.current-fragment {
			background-color: cyan;
		}
		.reveal .slides section .highlight-current-bg-magenta.current-fragment {
			background-color: magenta;
		}
		.reveal .slides section .fragment.highlight-current-bg-yellow.current-fragment {
			background-color: yellow;
		}
		</style>
	</head>
	<body>
		<div class="reveal">
			<div class="slides">
				<section>
					<h2>Applied Bayesian Data Analysis<h2>
							<h1>Priors</h1>
					<p><emph>David Tolpin, <a href="mailto:david.tolpin@gmail.com">david.tolpin@gmail.com</a></p>
					<h3>Concepts</h3>
					<ul>
						<li>Conjugacy</li>
						<li>Informative, non-informative, semi-informative priors</li>
						<li>Pivotal quantities</li>
					</ul>
				</section>


				<section>
					<section>
						<h2>Conjugacy</h2>
						<p>A boring mathematical concept:</p>
						<ul>
							<li>$\mathcal{F}$ — class of sampling distributions $p(y|\theta)$</li>
							<li>$\mathcal{P}$ — class of prior distributions for $\theta$</li>
							<li class="fragment">$\mathcal{P}$ is conjugate for $\mathcal{F}$ if
								$$p(\theta|y) \in \mathcal{P}\mbox{ for all }p(\cdot|\theta) \in \mathcal{F}\mbox{ and }p(\cdot) \in \mathcal{P}$$
							</li>
							<div class="fragment">
								<li>There are <i>natural</i> families of $\mathcal{P}$</li>
								<li>Conjugate priors/posteriors are <i>interpretable</i></li>
							</div>
						</ul>
					</section>

					<section>
						<h2>Example: binomial model</h2>
						\begin{aligned}
							\theta &\sim \mathrm{Prior} \\
							y_{1:n}&\sim \mathrm{Bernoulli}(\theta)
						 \end{aligned}

						 <p>How to choose $\mathrm{Prior}$?</p>
							
						<ul>
							<li>$p(\theta|y) \propto p(y, \theta) = p(\theta)p(y|\theta)$</li>
							<li class="fragment">$p(y_{1:n}|\theta) = \theta^k(1 - \theta)^{n-k}$</li>
							<li class="fragment"><b>if</b> $p(\theta) \propto \theta^a(1-\theta)^b$<br/> <b>then</b> $p(\theta|y) \propto \theta^{a + k}(1 - \theta)^{b + n - k}$ — same form</li>
						</ul>
					</section>
					
					<section>
						<h2>Example: binomial model</h2>
						<ul>
							<li>$\mathrm{Beta}(\theta|\alpha, \beta) = \frac 1 {\mathrm{B}(\alpha, \beta)} \theta^{\alpha-1} (1 - \theta)^{\beta - 1}$</li>
							<li class="fragment">$\mathrm{Beta}(\alpha, \beta)$ is the <i>conjugate prior</i> for $\mathrm{Bernoulli}(\theta)$</li>
							<li class="fragment"><ul><li>$\alpha$ — number of ‘prior’ successes (heads),</li> <li>$\beta$ — number of ‘prior’ failures (tails).</li></ul></li>
							<li class="fragment">$\alpha=\beta=1$ — uniform $[0, 1]$ prior.</li>
						</ul>
					</section>

					<section>
						<h2>Exponential families</h2>
						<ul>
							<li>$\mathcal{F}$ is an exponential family if
								$$p(y_i|\theta) = f(y_i)g(\theta)e^{\phi(\theta)^\top u(y_i)}$$
							</li>
							<li>$\phi(\theta)$ — <i>natural parameter</i></li>
							<div class="fragment">
								<li>likelihood of set $y=(y_1, ..., y_n)$ is
									$$p(y|\theta) \propto g(\theta)^ne^{\phi(\theta)^Tt(y)}$$
									where $t(y) = \sum_{i=1}^n u(y_i)$
								</li>
								<li>$t(y)$ is a <i>sufficient statistics</i> for $\theta$, all we need to know about the data</li>
							</div>
						</ul>
					</section>

					<section>
						<h2>Exp. family conjugates</h2>
						<ul>
							<li><b>If</b> $p(\theta) \propto g(\theta)^\eta e^{\phi(\theta)^T\nu}$,</li>
							<li class="fragment"><b>then</b> $p(\theta|y) \propto g(\theta)^{\eta+n}e^{\phi(\theta)^T(\nu+t(y))}$.</li> 
							<li class="fragment">$p(\theta|y)$ has the same form, so $p(\theta)$ is <b>conjugate</b> to $p(y|\theta)$.</li>
						</ul>
					</section>

					<section>
						<h2>Exp. family members</h2>
						<ul>
							<li>Bernoulli</li>
							<li>Normal, $\propto \frac 1 \sigma e^{-\frac 1 {2\sigma^2} {(x-\mu)^2}}$</li>
							<li>Poisson, $\propto \theta^y e^{-\theta}$</li>
							<li>Exponential, $\propto \theta e^{-y\theta}$</li>
							<li><a href="https://en.wikipedia.org/wiki/Conjugate_prior#Table_of_conjugate_distributions">...</a></li>
						</ul>
					</section>
				</section>

				<section>
					<section>
						<h2>Specifying priors</h2>

						<ul>
							<li>Prior $p(\theta) = \int_Y p(\theta|y)p(y)dy$ is marginal of $\theta$ over <i>all possible</i> observations.</li>
							<li>Posterior is a <i>compromise</i> between prior and conditional:
								<ul>
									<li>$\mathbb{E}(\theta) = \mathbb{E}(\mathbb{E}(\theta|y))$</li>
									<li>$\mathbb{E}(\mathbb{var}(\theta)) = \mathbb{E}(\mathbb{var}(\theta|y)) + \mathbb{var}(\mathbb{E}(\theta|y))$
										<ul>
											<li>$\mathbb{E}(\mathbb{var}(\theta|y))$ &mdash; ‘unexplained’ variation</li>
											<li>$\mathbb{var}(\mathbb{E}(\theta|y))$ &mdash; ‘explained’ variation</li> 
										</ul>
									</li>
								</ul>
							</li>
							<div class="fragment">
							<li>Posterior variance is <i>on average</i> smaller than prior</li>
							<li>If posterior variance is greater, look for a problem</li>
							</div>
						</ul>
					</section>
					<section>
						<h2>Informative priors</h2>
						<ul>
							<li>Prior defines the ‘population’</li>
							<li>Or, prior defines the ‘state of knowledge’</li>
							<li class="fragment">Example: coin flip
								<ul>
									<li>9+1 coins from the <b>same batch</b></li>
									<li>5 fell on heads, 4 on tails on a single toss</li>
									<li>Prior for the 10th coin: $\mathrm{Beta}(5, 4)$</li>
								</ul>
							</li>
						</ul>
					</section>

					<section>
						<h2>Non-informative priors</h2>
						<ul>
							<li>No prior information, (almost) all distributions are possible</li>
							<li>May be also used for ‘regularization’ that is, making model work</li>
							<li class="fragment">Examples:
								<ul>
									<li>$\mathrm{Beta}(1, 1)$ — uniform prior</li>
									<li>$\mathrm{Normal}(0, 1000)$ — regularization</li>
								</ul>
							</li>
						</ul>
					</section>

					<section>
						<h2>Non-informative priors</h2>
						<h3>Pivotal quantity, location, scale</h3>

						<ul>
							<li>Location:
								<ul>
									<li>$p(y-\theta|\theta) = f(u)$, $u = y - \theta$</li>
									<li>$y - \theta$ — pivotal quantity, $\theta$ — location parameter</li>
									<li>$p(\theta) \propto C$</li>
								</ul>
							</li>
							<li>Scale:
								<ul>
									<li>$p(\frac y \theta|\theta) = f(u)$, $u  = \frac y \theta$</li>
									<li>$\frac y \theta$ — pivotal quantity, $\theta$ - scale parameter</li>
									<li>$p(\log \theta) \propto C$, $p(\theta) \propto \frac 1 \theta$</li>
								</ul>
							</li>
						</ul>
					</section>

					<section>
						<h2>Weakly-informative priors</h2>
						<ul>
							<li>Some information</li>
							<li><i>Less</i> information than in the data</li>
							<li class="fragment">Examples:
								<ul>
									<li>Covered by water: $\mathrm{Uniform}(0.5, 1)$</li>
									<li>Salary: $\mathrm{Exponential}(11\,500₪)$</li>
								</ul>
							</li>
						</ul>
					</section>
				</section>

				<section>
					<h2>Readings</h2>

					<ol>
						<li style="font-size: 100%"><a href="http://www.stat.columbia.edu/~gelman/book/">Bayesian Data Analysis</a> — Chapter 2: Single-parameter models.</li>
						<li style="font-size: 100%"><a href="https://xcelab.net/rm/statistical-rethinking/">Statistical rethinking</a> — Sections 2.3: Components of the model, 2.4: Making the model go.</li>
					</ol>
				</section>


				<section>
					<h2>Hands-on</h2>
					<ul>
						<li>Circle or square?</li>
					</ul>
				</section>
			</div>
		</div>

		<script src="lib/js/head.min.js"></script>
		<script src="js/reveal.js"></script>

		<script>
			// More info about config & dependencies:
			// - https://github.com/hakimel/reveal.js#configuration
			// - https://github.com/hakimel/reveal.js#dependencies
			Reveal.initialize({
				transition: 'fade',
				math: {
					mathjax: 'https://cdn.jsdelivr.net/npm/mathjax@2.7.8/MathJax.js',
					config: 'TeX-AMS_HTML-full'
				},
				dependencies: [
					{ src: 'plugin/markdown/marked.js' },
					{ src: 'plugin/markdown/markdown.js' },
					{ src: 'plugin/notes/notes.js', async: true },
					{ src: 'plugin/highlight/highlight.js', async: true, callback: function() { hljs.initHighlightingOnLoad(); } },
					{ src: 'plugin/math/math.js', async: true }
				]
			});
		</script>
	</body>
</html>
